// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"controllers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
	"time"
)

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		MaxAge:           5 * time.Minute,
		AllowCredentials: true,
	}))

	beego.Router("/", &controllers.MainController{})
	ns := beego.NewNamespace("/v1",

		beego.NSNamespace("/asset",
			beego.NSInclude(
				&controllers.AssetController{},
			),
		),

		beego.NSNamespace("/asset_area",
			beego.NSInclude(
				&controllers.AssetAreaController{},
			),
		),

		beego.NSNamespace("/asset_company",
			beego.NSInclude(
				&controllers.AssetCompanyController{},
			),
		),

		beego.NSNamespace("/asset_type",
			beego.NSInclude(
				&controllers.AssetTypeController{},
			),
		),

		beego.NSNamespace("/system_role",
			beego.NSInclude(
				&controllers.SystemRoleController{},
			),
		),

		beego.NSNamespace("/system_user",
			beego.NSInclude(
				&controllers.SystemUserController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
