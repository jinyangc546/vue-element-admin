package controllers

import (

)

type MainController struct {
	BaseRouter
}

func (c *MainController) Get() {
	c.TplName = "index.tpl"
}
