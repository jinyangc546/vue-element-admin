package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type Asset struct {
	Id           int       `orm:"column(AsId);auto"`
	AsName       string    `orm:"column(AsName);size(255)"`
	TypeId       uint      `orm:"column(TypeId)"`
	AsCode       string    `orm:"column(AsCode);size(255)"`
	AsSn         string    `orm:"column(AsSn);size(255);null"`
	AsBrand      string    `orm:"column(AsBrand);size(255);null"`
	AsSpec       string    `orm:"column(AsSpec);null"`
	AsAllocation string    `orm:"column(AsAllocation);null"`
	AsUnit       string    `orm:"column(AsUnit);size(255);null"`
	AsAmount     string    `orm:"column(AsAmount);size(255);null"`
	AsOriginal   string    `orm:"column(AsOriginal);size(255);null"`
	AsBelongComp string    `orm:"column(AsBelongComp);size(255)"`
	AsUsedComp   string    `orm:"column(AsUsedComp);size(255);null"`
	AsUsedDep    string    `orm:"column(AsUsedDep);size(255);null"`
	AsUsedPerson string    `orm:"column(AsUsedPerson);size(255);null"`
	AsManager    string    `orm:"column(AsManager);size(255);null"`
	AsFrom       string    `orm:"column(AsFrom);size(255);null"`
	AsArea       string    `orm:"column(AsArea);size(255)"`
	AsLocation   string    `orm:"column(AsLocation);size(255);null"`
	AsSupplier   string    `orm:"column(AsSupplier);size(255);null"`
	AsRemart     string    `orm:"column(AsRemart);null"`
	AsPic        string    `orm:"column(AsPic);size(255);null"`
	AsBuyTime    time.Time `orm:"column(AsBuyTime);type(date)"`
	AsTimeLimit  string    `orm:"column(AsTimeLimit);size(255);null"`
	AsStatus     string    `orm:"column(AsStatus);size(255);null"`
	AsUpdate     time.Time `orm:"column(AsUpdate);type(timestamp);null"`
}

func (t *Asset) TableName() string {
	return "asset"
}

func init() {
	orm.RegisterModel(new(Asset))
}

// AddAsset insert a new Asset into database and returns
// last inserted Id on success.
func AddAsset(m *Asset) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetAssetById retrieves Asset by Id. Returns error if
// Id doesn't exist
func GetAssetById(id int) (v *Asset, err error) {
	o := orm.NewOrm()
	v = &Asset{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllAsset retrieves all Asset matches certain condition. Returns empty list if
// no records exist
func GetAllAsset(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(Asset))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []Asset
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateAsset updates Asset by Id and returns error if
// the record to be updated doesn't exist
func UpdateAssetById(m *Asset) (err error) {
	o := orm.NewOrm()
	v := Asset{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteAsset deletes Asset by Id and returns error if
// the record to be deleted doesn't exist
func DeleteAsset(id int) (err error) {
	o := orm.NewOrm()
	v := Asset{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&Asset{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}

// GetAssetByAsCode retrieves Asset by AsCode. Returns error if
// Id doesn't exist
func GetAssetByAsCode(code string) (l []Asset, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(Asset))
	qs = qs.Filter("AsCode", code)
	if _, err = qs.All(&l); err == nil {
		return l, nil
	} else {
		return l, err
	}
}