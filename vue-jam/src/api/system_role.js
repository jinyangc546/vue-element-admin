import fetch from 'utils/fetch';

export function fetchList() {
  return fetch({
    url: '/v1/system_role',
    method: 'get',
  });
}

export function addRole(data) {
  return fetch({
    url: '/v1/system_role',
    method: 'post',
    data
  });
}

export function delRole(id) {
  return fetch({
    url: '/v1/system_role/'+id,
    method: 'delete',
  });
}

export function updateRole(data) {
  return fetch({
    url: '/v1/system_role/'+data.Id,
    method: 'put',
    data
  });
}