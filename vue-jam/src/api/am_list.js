import fetch from 'utils/fetch';

export function postAsset(data) {
  return fetch({
    url: '/v1/asset',
    method: 'post',
    data
  });
}

export function fetchList(query) {
  return fetch({
    url: '/v1/asset',
    method: 'get',
    params: query
  });
}

export function checkAsCode(code) {
  return fetch({
    url: '/v1/asset/checkAsCode/'+code,
    method: 'get',
  });
}

