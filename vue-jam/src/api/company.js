import fetch from 'utils/fetch';

export function getList() {
    return fetch({
        url: 'v1/asset_company',
        method: 'get'
    });
}

export function postData(data) {
    var data =  JSON.stringify(data);
    return fetch({
        url: 'v1/asset_company',
        method: 'post',
        data
    });
}

export function delData(id) {
    console.log(id);
    return fetch({
        url: 'v1/asset_company/'+id,
        method: 'Delete'
    });
}

export function updateData(data) {
    var data =  JSON.stringify(data);
    console.log(data);
    return fetch({
        url: 'v1/asset_company/'+data.id,
        method: 'put',
        data
    });
}

