import fetch from 'utils/fetch';

export function getList() {
  return fetch({
    url: 'v1/asset_area',
    method: 'get'
  });
}

