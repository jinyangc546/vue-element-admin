

**卷皮资产管理系统**


## 开发
```bash
    # 克隆项目
    git clone https://gitee.com/jinyangc546/vue-element-admin.git

    # 安装vue模块
    cd vue-jam
   
    npm install
    //or # 建议不要用cnpm  安装有各种诡异的bug 可以通过如下操作解决npm速度慢的问题
    npm install --registry=https://registry.npm.taobao.org

    # 本地开发 开启服务
    npm run dev
    
    cd src
    bee run
```
浏览器访问 http://localhost:8880



